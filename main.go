package main

import (
"fmt"
"log"
"net"
	"os"

	"github.com/olekukonko/tablewriter"
)

func main() {
	localAddresses()
}

func localAddresses() {
	ifaces, err := net.Interfaces()
	if err != nil {
		log.Print(fmt.Errorf("localAddresses: %v\n", err.Error()))
		return
	}

	var interfaces [][]string

	for _, iFace := range ifaces {
		addresses, err := iFace.Addrs()
		if err != nil {
			log.Print(fmt.Errorf("localAddresses: %v\n", err.Error()))
			continue
		}

		// ignore if interface is down
		if iFace.Flags&net.FlagUp == 0 {
			// interface is up
			continue
		}
		// ignore if interface cannot broadcast
		if iFace.Flags&net.FlagBroadcast == 0 {
			// interface has broadcast
			continue
		}
		// ignore if interface is loopback
		if iFace.Flags&net.FlagLoopback != 0 {
			// interface has loopback
			continue
		}
		// check if interface can multicast
		if iFace.Flags&net.FlagMulticast != 0 {
			// interface has multicast
			// continue
		}
		// ignore if interface is P2P
		if iFace.Flags&net.FlagPointToPoint != 0 {
			// interface has p2p
			continue
		}

		for _, address := range addresses {
			ip, ipNet, err := net.ParseCIDR(address.String())
			if err != nil {
				continue
			}

			if ip.IsMulticast() || ip.IsUnspecified() {
				continue
			}

			var mask string
			if ip.To4() != nil {
				mask = ipv4MaskString(ipNet.Mask)
			} else {
				bits, _ := ipNet.Mask.Size()
				mask = fmt.Sprintf("%d", bits)
			}

			// var name string
			// if len(ifaces) == 1 {
			// 	name = fmt.Sprintf("%v\n%v", iFace.Name, iFace.HardwareAddr)
			// } else {
			// 	if i == 0 {
			// 		name = iFace.Name
			// 	} else if i == 1 {
			// 		name = iFace.HardwareAddr.String()
			// 	}
			// }

			interfaces = append(interfaces, []string{fmt.Sprintf("%v\n%v", iFace.Name, iFace.HardwareAddr), address.String(), mask})
			// fmt.Printf("%v\t\t%v %v %v\n", iFace.Name, iFace.HardwareAddr, ip, ipNet)
		}
	}

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Interface Name", "IP", "MASK"})

	for _, v := range interfaces {
		table.Append(v)
	}
	table.SetAutoMergeCells(true)
	table.SetRowLine(true)
	table.Render()
}

func ipv4MaskString(m []byte) string {
	if len(m) != 4 {
		panic("ipv4Mask: len must be 4 bytes")
	}

	return fmt.Sprintf("%d.%d.%d.%d", m[0], m[1], m[2], m[3])
}
